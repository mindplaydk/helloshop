﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HelloShopLib
{
    public interface Identity
    {
        Guid Id { get; }
    }

    public class Product : Identity
    {
        public Guid Id { get; protected set; }

        public String Name { get; protected set; }

        public decimal Price { get; protected set; }

        public Product(Guid id, String name, decimal price)
        {
            Id = id;
            Name = name;
            Price = price;
        }
    }

    public class Repository<T>
        where T : Identity
    {
        Dictionary<Guid, T> items = new Dictionary<Guid, T>();

        public T Get(Guid id)
        {
            if (items.ContainsKey(id)) {
                return items[id];
            }

            throw new ArgumentOutOfRangeException();
        }

        public void Put(T item)
        {
            items[item.Id] = item;
        }
    }

    public class ProductRepository : Repository<Product>
    { }

    public class Order
    {
        public List<OrderLine> Lines { get; protected set; }

        public Order(List<OrderLine> lines)
        {
            Lines = lines;
        }
    }

    public class OrderLine
    {
        public Guid ProductId { get; protected set; }

        public int Amount { get; set; }

        public OrderLine(Guid productId, int amount)
        {
            ProductId = productId;
            Amount = amount;
        }
    }

    public class Total
    {
        public List<TotalLine> Lines { get; }

        public decimal Value
        {
            get
            {
                return Lines.Aggregate(0m, (total, line) => total + line.Value);
            }
        }

        public Total()
        {
            Lines = new List<TotalLine>();
        }

        public void Add(String description, decimal value)
        {
            Lines.Add(new TotalLine(description, value));
        }
    }

    public class TotalLine
    {
        public String Description { get; protected set; }

        public decimal Value { get; protected set; }

        public TotalLine(string description, decimal value)
        {
            Description = description;
            Value = value;
        }
    }

    public class PricingService
    {
        List<Pricing> pricings { get; }

        public PricingService(List<Pricing> pricings)
        {
            this.pricings = pricings;
        }

        public Total GetTotal(Order order)
        {
            var total = new Total();

            foreach (var pricing in pricings) {
                pricing.Update(total, order);
            }

            return total;
        }
    }

    public interface Pricing
    {
        void Update(Total total, Order order);
    }

    public class ProductPricing : Pricing
    {
        ProductRepository products;

        public void Update(Total total, Order order)
        {
            decimal summarize(decimal value, OrderLine line) {
                return value + line.Amount * products.Get(line.ProductId).Price;
            }

            total.Add(
                "Subtotal",
                order.Lines.Aggregate(0m, summarize)
            );
        }

        public ProductPricing(ProductRepository products)
        {
            this.products = products;
        }
    }

    public class TaxPricing : Pricing
    {
        decimal taxRate;

        public void Update(Total total, Order order)
        {
            total.Add($"Taxes ({taxRate}%)", total.Value * taxRate * 0.01m);
        }

        public TaxPricing(decimal taxRate)
        {
            this.taxRate = taxRate;
        }
    }
}
