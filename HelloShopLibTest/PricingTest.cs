using DeepEqual.Syntax;
using HelloShopLib;
using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace HelloShopLibTest
{
    public class PricingTest
    {
        private readonly ITestOutputHelper output;

        public PricingTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            var products = new ProductRepository();

            var shoes = new Product(Guid.NewGuid(), "Shoes", 25);
            var shirt = new Product(Guid.NewGuid(), "Shirt", 15);

            products.Put(shoes);
            products.Put(shirt);

            Assert.StrictEqual(shoes, products.Get(shoes.Id));
            Assert.StrictEqual(shirt, products.Get(shirt.Id));

            var order = new Order(new List<OrderLine> {
                new OrderLine(shirt.Id, 1),
                new OrderLine(shoes.Id, 2),
            });

            Assert.Equal(2, order.Lines.Count);

            var pricing = new PricingService(new List<Pricing> {
                new ProductPricing(products),
                new TaxPricing(25m),
            });

            var total = pricing.GetTotal(order);

            foreach (var line in total.Lines)
            {
                output.WriteLine($"{line.Description} : {line.Value}");
            }

            Assert.Equal(81.25m, total.Value);

            Assert.True(total.Lines[0].IsDeepEqual(new TotalLine("Subtotal", 65m)));
            Assert.True(total.Lines[1].IsDeepEqual(new TotalLine("Taxes (25%)", 16.25m)));
        }
    }
}
