﻿using System;
using System.IO;
using System.Threading.Tasks;
using Funq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using ServiceStack;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using HelloShopLib;
using System.Collections.Generic;
using HelloShopServices;

namespace HelloShopApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseModularStartup<Startup>()
                .UseUrls(Environment.GetEnvironmentVariable("ASPNETCORE_URLS") ?? "http://localhost:5000/")
                .Build();

            host.Run();
        }
    }

    public class Startup : ModularStartup
    {
        public new void ConfigureServices(IServiceCollection services)
        {
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseServiceStack(new AppHost());

            app.Run(context =>
            {
                context.Response.Redirect("/metadata");

                return Task.FromResult(0);
            });
        }
    }

    public class AppHost : AppHostBase
    {
        public AppHost()
            : base("HelloShop", typeof(PricingTotalService).Assembly) { }

        public override void Configure(Container container)
        {
            container.Register<ProductRepository>(c =>
            {
                var products = new ProductRepository();

                var shoes = new Product(new Guid("b440b8e6-2924-46f7-a4ba-2ceff538c8f0"), "Shoes", 25);
                var shirt = new Product(new Guid("9507ccc2-824c-444e-98eb-b743b1919eb4"), "Shirt", 15);

                return products;
            });

            container.Register<PricingService>(c => new PricingService(
                new List<Pricing> {
                    new ProductPricing(c.Resolve<ProductRepository>()),
                    new TaxPricing(25m),
                }
            ));
        }
    }
}
