﻿using HelloShopLib;
using ServiceStack;

namespace HelloShopServices
{
    public class PricingTotalRequest : IReturn<PricingTotalResponse>
    {
        public Order Order { get; }

        public PricingTotalRequest(Order order)
        {
            Order = order;
        }
    }

    public class PricingTotalResponse
    {
        public Total Total { get; }

        public PricingTotalResponse(Total total)
        {
            Total = total;
        }
    }

    public class PricingTotalService : IGet<PricingTotalRequest>
    {
        PricingService pricing;

        PricingTotalService(PricingService pricing)
        {
            this.pricing = pricing;
        }

        public object Get(PricingTotalRequest request)
        {
            return new PricingTotalResponse(
                pricing.GetTotal(request.Order)
            );
        }
    }
}
